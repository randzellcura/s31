// 1. What directive is used by Node.js in loading the modules it needs?

// require()

// 2. What Node.js module contains a method for server creation?

// http module

// 3. What is the method of the http object responsible for creating a server using Node.js?

// createServer() method

// 4. What method of the response object allows us to set status codes and content types?

// writehead

// 5. Where will console.log() output its contents when run in Node.js?

// gitbash/terminal/cli

// 6. What property of the request object contains the address's endpoint?

//  response.url

let http = require("http")

const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Hi Ivan');

    }
    if (req.url === '/login') {
        res.write('Welcome to the login page');
        res.end();
    }
    if (req.url === '/register') {
        res.write("I'm sorry the page you are looking for cannot be found.");
        res.end();
    }
});

server.listen(3000);
console.log('Server is running at localhost:3000');